$( "#menu" ).click(function() {
  $( ".overlay" ).css("opacity", " 0.5");
  $( ".left-menu" ).removeClass("_close")
});

$( "#close-left-menu" ).click(function() {
  $( ".overlay" ).css("opacity", "0");
  $( ".left-menu" ).addClass("_close")
});
$( "#seaarch" ).click(function() {
  $( ".list-options" ).css("display", "block");
});
$( "#triangle" ).click(function() {
  $( ".list-year" ).css("display", "inline-block");
});
$(window).load(function(){
  const x = window.location.pathname.split("/")
  const pathname = x[x.length-1];
  if(
    pathname == "page-13.html"
    ||
    pathname == "page-12.html"
    ||
    pathname == "page-14.html"
    ||
    pathname == "page-17.html"
    ||
    pathname == "page-16.html"
    ||
    pathname == "page-18.html"
    ||
    pathname == "page-19.html"
    ||
    pathname == "page-20.html"
    ||
    pathname == "page-21.html"
    ||
    pathname == "page-28.html"
  ){
    $( ".overlay" ).css("opacity", " 0.5");
    $( ".modal" ).removeClass("_close")
  }
});

$( "#close-modal" ).click(function() {
  $( ".overlay" ).css("opacity", "0");
  $( ".modal" ).addClass("_close")
});

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
  $('body,html').css('backgroundSize','inherit');
}
else{
  $('body,html').css('backgroundSize','cover');
}



let size = 56;
let sizeHoliday = 60;
let newsContent = document.querySelectorAll('.block .profile._sv .name') //ИМЕННИНЫ СЕГОДНЯ
let newsContentHoliday = document.querySelectorAll('.holiday .right .text') //БЛИЖАЙШИЕ ПРАЗДНИКИ
newsContent.forEach((item, index) => {
  let newsText = item.textContent.trim();
  if (newsText.length > size) {
    newsContent[index].innerText = (newsText.slice(0, size) + " ...");
  }
});
newsContentHoliday.forEach((item, index) => {

  let newsText = item.textContent.trim();

  if (newsText.length > sizeHoliday) {
    newsContentHoliday[index].innerText = (newsText.slice(0, sizeHoliday) + " ...");
  }
});
